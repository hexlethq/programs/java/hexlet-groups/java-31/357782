package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";
        result += str.charAt(0);
        int length = str.length();
        char symbol = ' ';
        for (int i = 1; i < length; i++) {
            if (str.charAt(i - 1) == symbol) {
                result += Character.toUpperCase(str.charAt(i));
            }
        }
        return result.replaceAll("\\s", "");
    }
    // END
}
