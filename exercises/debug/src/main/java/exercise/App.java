package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int num1, int num2, int num3) {
        boolean isExist = false;
        String result = "";
        if (num1 + num2 > num3 && num2 + num3 > num1 && num3 + num1 > num2) {
            isExist = true;
        } else if (!isExist) {
            result = "Треугольник не существует";
        }
        if ((isExist) && (num1 != num2 && num2 != num3 && num3 != num1)) {
            result = "Разносторонний";
        } else if ((isExist) && (num1 == num2 && num2 == num3)) {
            result = "Равносторонний";
        } else if ((isExist) && (num1 == num2 && num2 != num3) || (num1 == num3 && num3 != num2) || (num2 == num3 && num3 != num1)) {
            result = "Равнобедренный";
        }
        return result;
    }
    // END
}
