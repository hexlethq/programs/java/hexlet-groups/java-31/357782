package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        var count = "*".repeat(starsCount);
        var end = cardNumber.substring(12);
        var result = count + end;
        System.out.println(result);
        // END
    }
}
