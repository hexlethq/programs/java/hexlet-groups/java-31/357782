package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int firstSide, int secSide, int corner) {
        final double pi = 3.14;
        double r = corner * pi / 180;
        double sin = Math.sin(r);
        double square = firstSide * secSide / 2 * sin;
        return (square);
    }
    public static void main (String[] args) {
        System.out.println (getSquare (4, 5, 45));
    }

    // END
}
